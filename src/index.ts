import { ApolloServer } from "apollo-server-express";
import dotenv from "dotenv";
import express from "express";
import mongoose from "mongoose";
import schema from "./graphql/schemasMap";
import { decodeToken } from "./utils/jwt";
dotenv.config();

async function init() {
  try {
    const PORT: number = parseInt(process.env.PORT || "5000");
    const DB_URI: string = process.env.DB_URI || "";

    const app = express();

    const server = new ApolloServer({
      schema,
      context: async ({ req }) => {
        const header = req.headers["authorization"];
        if (!header) return { me: null };

        const token = header.split("Bearer")[1].trim();
        if (!token) return { me: null };

        const user = decodeToken(token);
        return { me: user };
      },
      introspection: process.env.NODE_ENV !== "production",
    });

    await server.start();
    server.applyMiddleware({
      app,
      path: "/graphql",
    });

    await mongoose.connect(DB_URI);
    console.log("[APP]", "Database Connected");

    app.listen(PORT, () => {
      console.log("[APP]", `GraphQL active: http://localhost:${PORT}/graphql`);
    });
  } catch (err) {
    console.log(err);
  }
}

init();
