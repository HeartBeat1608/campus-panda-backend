export interface IPaymentMethodType {
  _id: string;
  name: string;
  type: string;
  card_number: string;
  card_type: string;
  expiry: string;
  bank: string;
}
