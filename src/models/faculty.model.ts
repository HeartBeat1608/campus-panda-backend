import bcrypt from "bcrypt";
import { model, Model, Schema } from "mongoose";
import { IPaymentMethodType } from "./shared.types";

const Types = Schema.Types;

export interface IFacultyType {
  // Basic Information
  _id: string;
  email: string;
  name: string;
  password: string;
  address?: string;

  // in-campus Information
  department?: string;
  floor?: string;
  cabin?: string;
  block?: string;

  // payment Information
  payment_methods: IPaymentMethodType[];
  orders: string[];

  // methods
  serialize(): IFacultyType & { _id: string; id: string };
}

interface IFacultyModel extends Model<IFacultyType> {
  serialize(): IFacultyType & { _id: string; id: string };
}

const FacultySchema = new Schema<IFacultyType, IFacultyModel>(
  {
    // Basic Information
    email: { type: Types.String, required: true, unique: true },
    name: { type: Types.String, required: true },
    password: { type: Types.String, required: true },

    // in-campus Information
    department: { type: Types.String, default: "" },
    floor: { type: Types.String, default: "" },
    cabin: { type: Types.String, default: "" },
    block: { type: Types.String, default: "" },

    // payment Information
    payment_methods: [
      {
        name: Types.String,
        type: Types.String,
        card_number: Types.String,
        card_type: Types.String,
        expir: Types.String,
        bank: Types.String,
      },
    ],
    orders: [
      {
        type: Types.ObjectId,
        ref: "Order",
      },
    ],
  },
  { timestamps: true }
);

FacultySchema.pre("save", async function () {
  const hashedPassword = await bcrypt.hash(this.password, 16);
  this.password = hashedPassword;
});

FacultySchema.method("serialize", function () {
  return {
    ...this.toObject(),
    _id: this._id.toString(),
    id: this._id.toString(),
  };
});

export default model<IFacultyType, IFacultyModel>("Faculty", FacultySchema);
