import jwt from "jsonwebtoken";

const JWT_SECRET: string = process.env.JWT || "";

export const generateToken = (payload: any) => {
  try {
    return jwt.sign(JSON.stringify(payload), JWT_SECRET, { expiresIn: "30d" });
  } catch (err: any) {
    console.log(err);
    return null;
  }
};

export const decodeToken = (token: string): object | null => {
  try {
    return JSON.parse(
      jwt.verify(token, JWT_SECRET, { complete: true }).toString()
    );
  } catch (err: any) {
    console.log(err);
    return null;
  }
};
