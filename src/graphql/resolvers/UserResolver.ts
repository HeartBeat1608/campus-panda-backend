import { IResolvers } from "@graphql-tools/utils";
import {
  AuthenticationResponse,
  QueryLoginArgs,
  MutationRegisterArgs,
  QueryGetFacultyByIdArgs,
} from "../generated";
import { generateToken } from "../../utils/jwt";
import facultyModel from "../../models/faculty.model";
import { ApolloError } from "apollo-server-express";

export const FacultyResolvers: IResolvers = {
  Query: {
    login: async (
      _: void,
      { email, password }: QueryLoginArgs
    ): Promise<AuthenticationResponse> => {
      const faculty = await facultyModel.findOne({ email, password });

      if (!faculty) throw new ApolloError("Invalid Email or Password");
      const token = generateToken(faculty.serialize());

      if (!token) throw new ApolloError("Could not generate a valid token.");

      return {
        token,
        faculty: faculty.serialize(),
      };
    },
    getAllFaculties: async () => {
      return await facultyModel.find();
    },
    getFacultyById: async (_: void, args: QueryGetFacultyByIdArgs) => {
      return await facultyModel.findById(args.id);
    },
    myProfile: async (_: void, _args: any, context: any) => {
      return {};
    },
  },
  Mutation: {
    register: async (
      _: void,
      args: MutationRegisterArgs
    ): Promise<AuthenticationResponse> => {
      const { email, password, name } = args;

      try {
        const faculty = await new facultyModel({
          email,
          password,
          name,
        }).save({ validateBeforeSave: true });

        const token: string | null = generateToken({
          name: faculty.name,
          email: faculty.email,
          department: faculty.department,
          cabin: faculty.cabin,
          floor: faculty.floor,
        });

        if (!token) throw new Error("Could Not generate a valid Token");

        return {
          token,
          faculty: faculty.serialize(),
        };
      } catch (err: any) {
        throw new ApolloError(err.message);
      }
    },
  },
};
