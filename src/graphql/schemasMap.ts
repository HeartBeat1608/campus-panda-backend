import "graphql-import-node";
import * as emptyTypedefs from "./schemas/empty.graphql";
import * as userTypedefs from "./schemas/user.graphql";
import * as sharedTypedefs from "./schemas/shared.graphql";
import { makeExecutableSchema } from "@graphql-tools/schema";
import resolvers from "./resolversMap";
import { GraphQLSchema } from "graphql";

const schema: GraphQLSchema = makeExecutableSchema({
  typeDefs: [emptyTypedefs, sharedTypedefs, userTypedefs],
  resolvers,
});

export default schema;
