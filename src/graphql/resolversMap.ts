import { IResolvers } from "@graphql-tools/utils";
import { merge } from "lodash";
import { FacultyResolvers } from "./resolvers/UserResolver";

const resolversMap: IResolvers = merge(FacultyResolvers);
export default resolversMap;
